create database dbconnections;
create sequence hibernate_sequence start with 1 increment by 1;
CREATE TABLE dbconnection (
    id serial PRIMARY KEY,
    name varchar(50) unique not null,
    username VARCHAR ( 50 ),
    password VARCHAR ( 50 ),
    hostname VARCHAR ( 50 ),
    port VARCHAR ( 50 ) not null,
    database_name VARCHAR ( 50 ) not null
);

CREATE TABLE users (
    id serial PRIMARY KEY,
    username VARCHAR ( 120 ) unique,
    password VARCHAR ( 120 )
);