package cz.pkupka.dbbrowser.helpers;

import cz.pkupka.dbbrowser.models.DBConnection;

/**
 * Builder class for building urls
 *
 * @author PKupka27
 *
 */
public class URLBuilder {

    /**
     * <p>Builds url from db connection object
     * </p>
     * @param dbConnection connection description object
     * @return url string
     */
    public static String buildURL(DBConnection dbConnection) {
        String url = "jdbc:postgresql://" + dbConnection.getHostname()
                + ":" + dbConnection.getPort()
                + "/" + dbConnection.getDatabaseName()
                + "?user=" + dbConnection.getUsername()
                + "&password=" + dbConnection.getPassword();
        if (!(dbConnection.getSchema() == null || dbConnection.getSchema() == "")) {
            url = url + "&currentSchema=" + dbConnection.getSchema();
        }
        return url;
    }
}
