package cz.pkupka.dbbrowser.helpers;

import cz.pkupka.dbbrowser.models.dto.ColumnDTO;
import cz.pkupka.dbbrowser.models.dto.SchemaDTO;
import cz.pkupka.dbbrowser.models.dto.TableDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * Builder class for building entities from result set
 *
 * @author PKupka27
 */
public class EntityBuilder {

    /**
     * <p>Builds schemas data from result set
     * </p>
     *
     * @param result result set get from database
     * @return Set of found schemas
     */
    public static Set getSchemas(ResultSet result) throws SQLException {
        Set<SchemaDTO> schemas = new HashSet<>();
        while (result.next()) {
            SchemaDTO schemaDTO = new SchemaDTO();
            schemaDTO.setCatalogName(result.getString("catalog_name"));
            schemaDTO.setSchemaName(result.getString("schema_name"));
            schemaDTO.setSchemaOwner(result.getString("schema_owner"));
            schemas.add(schemaDTO);
        }
        return schemas;
    }

    /**
     * <p>Builds tables data from result set
     * </p>
     *
     * @param result result set get from database
     * @return Set of found tables
     */
    public static Set getTables(ResultSet result) throws SQLException {
        Set<TableDTO> tables = new HashSet<>();
        while (result.next()) {
            TableDTO tableDTO = new TableDTO();
            tableDTO.setTableCatalog(result.getString("table_catalog"));
            tableDTO.setTableName(result.getString("table_name"));
            tableDTO.setTableSchema(result.getString("table_schema"));
            tableDTO.setTableType(result.getString("table_type"));
            tables.add(tableDTO);
        }
        return tables;
    }

    /**
     * <p>Builds columns from result set
     * </p>
     *
     * @param result result set get from database
     * @return Set of found columns
     */
    public static Set getColumns(ResultSet result) throws SQLException {
        Set<ColumnDTO> columns = new HashSet<>();
        while (result.next()) {
            ColumnDTO columnDTO = new ColumnDTO();
            columnDTO.setTableCatalog(result.getString("table_catalog"));
            columnDTO.setTableName(result.getString("table_name"));
            columnDTO.setTableSchema(result.getString("table_schema"));
            columnDTO.setColumnName(result.getString("column_name"));
            columnDTO.setColumnDefault(result.getString("column_default"));
            columnDTO.setDataType(result.getString("data_type"));
            columnDTO.setIsNullable(result.getString("is_nullable"));
            columnDTO.setOrdinalPosition(result.getInt("ordinal_position"));
            columns.add(columnDTO);
        }
        return columns;
    }
}
