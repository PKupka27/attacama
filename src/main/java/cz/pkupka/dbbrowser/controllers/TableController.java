package cz.pkupka.dbbrowser.controllers;

import cz.pkupka.dbbrowser.services.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TableController {

    @Autowired
    TableService tableService;

    @GetMapping("/tables/connectionName/{connectionName}")
    public ResponseEntity getTablesByConnectionName(@PathVariable String connectionName) {
        return tableService.getTablesByConnectionName(connectionName);
    }
}
