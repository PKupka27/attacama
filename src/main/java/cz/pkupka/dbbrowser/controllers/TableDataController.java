package cz.pkupka.dbbrowser.controllers;

import cz.pkupka.dbbrowser.services.TableDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TableDataController {

    @Autowired
    TableDataService tableDataService;

    @GetMapping("/table-data/{tableName}/connection-name/{connectionName}")
    public ResponseEntity getColumnsByConnectionName(@PathVariable String tableName, @PathVariable String connectionName) {
        return tableDataService.getTableData(tableName, connectionName);
    }
}
