package cz.pkupka.dbbrowser.controllers;

import cz.pkupka.dbbrowser.models.DBConnection;
import cz.pkupka.dbbrowser.repositories.DBConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DBConnectionController {

    @Autowired
    DBConnectionRepository dbConnectionRepository;

    @PostMapping("/connections")
    public DBConnection saveConnection(@RequestBody DBConnection dbConnection) {
        return dbConnectionRepository.save(dbConnection);
    }

    @GetMapping("/connections/{id}")
    public DBConnection getConnection(@PathVariable Long id) {
        return dbConnectionRepository.findById(id).get();
    }

    @DeleteMapping("/connections/{id}")
    public void deleteConnection(@PathVariable Long id) {
        dbConnectionRepository.deleteById(id);
    }
}
