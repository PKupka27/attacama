package cz.pkupka.dbbrowser.controllers;

import cz.pkupka.dbbrowser.services.ColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ColumnController {

    @Autowired
    ColumnService columnService;

    @GetMapping("/columns/connectionName/{connectionName}")
    public ResponseEntity getColumnsByConnectionName(@PathVariable String connectionName) {
        return columnService.getColumnsByConnectionName(connectionName);
    }
}
