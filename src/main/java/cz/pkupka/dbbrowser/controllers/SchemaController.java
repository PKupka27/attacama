package cz.pkupka.dbbrowser.controllers;

import cz.pkupka.dbbrowser.services.SchemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SchemaController {

    @Autowired
    SchemaService schemaService;

    @GetMapping("/schemas/connectionName/{connectionName}")
    public ResponseEntity getSchemasByConnectionName(@PathVariable String connectionName) {
        return schemaService.getSchemasByConnectionName(connectionName);
    }
}
