package cz.pkupka.dbbrowser.models;

public enum EntityType {
    SCHEMA,
    TABLE,
    COLUMN
}
