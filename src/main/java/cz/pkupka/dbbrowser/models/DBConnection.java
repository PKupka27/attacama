package cz.pkupka.dbbrowser.models;


import javax.persistence.*;

@Entity
public class DBConnection {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(unique = true)
    String name;
    String username;
    String password;
    String hostname;
    String port;
    String databaseName;
    String schema;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getHostname() {
        return hostname;
    }

    public String getPort() {
        return port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getSchema() {
        return schema;
    }
}
