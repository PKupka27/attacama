package cz.pkupka.dbbrowser.repositories;

import cz.pkupka.dbbrowser.helpers.EntityBuilder;
import cz.pkupka.dbbrowser.models.EntityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * DAO class for connections to database
 *
 * @author PKupka27
 */
@Repository
public class CustomDatabaseDAO {

    /**
     * string to get data from
     */
    private static final String SELECT_FROM_TABLE_QUERY = "SELECT * FROM ";

    /**
     * query string to get all schemas
     */
    private static final String SCHEMA_QUERY = SELECT_FROM_TABLE_QUERY + "information_schema.schemata;";
    /**
     * query string to get all tablse
     */
    private static final String TABLE_QUERY = SELECT_FROM_TABLE_QUERY + "information_schema.tables";
    /**
     * query string to get all columns
     */
    private static final String COLUMN_QUERY = SELECT_FROM_TABLE_QUERY + "information_schema.columns";
    /**
     * query string to get column name and type from specific table
     */
    private static final String COLUMN_NAME_QUERY_BY_TABLE = "SELECT column_name, data_type FROM information_schema.columns where table_name = ?";


    Logger logger = LoggerFactory.getLogger(CustomDatabaseDAO.class);

    /**
     * <p> Gets all entities from connection url passed
     * </p>
     *
     * @param url        connection url
     * @param entityType type of entity to get
     * @return set of entities
     */
    public Set getAllEntities(String url, EntityType entityType) throws SQLException {
        try (
                Connection connection = DriverManager.getConnection(url);
                PreparedStatement statement = connection.prepareStatement(getQueryByType(entityType));
                ResultSet result = statement.executeQuery()) {
            switch (entityType) {
                case SCHEMA:
                    return EntityBuilder.getSchemas(result);
                case TABLE:
                    return EntityBuilder.getTables(result);
                case COLUMN:
                    return EntityBuilder.getColumns(result);
                default:
            }
            return new HashSet<>();
        } catch (SQLException ex) {
            logger.error("Error in connecting to database");
            throw new SQLException();
        }
    }

    /**
     * <p> Gets all column names and their data types from specific table
     * </p>
     *
     * @param url       connection url
     * @param tableName name of table to get columns from
     * @return map of columns and data type
     */
    public Map getColumnsMap(String url, String tableName) throws SQLException {
        try (
                Connection connection = DriverManager.getConnection(url);
                PreparedStatement statement = connection.prepareStatement(COLUMN_NAME_QUERY_BY_TABLE);
        ) {
            statement.setString(1, tableName);
            ResultSet result = statement.executeQuery();
            Map<String, String> columns = new HashMap<String, String>();
            while (result.next()) {
                columns.put(result.getString("column_name"), result.getString("data_type"));
            }
            return columns;
        } catch (SQLException ex) {
            logger.error("Error in connecting to database");
            throw new SQLException();
        }
    }

    /**
     * <p> Gets data from table
     * </p>
     *
     * @param url       connection url
     * @param tableName name of table to get data from
     * @return Set of row data
     */
    public Set getDataOfTable(String url, String tableName) throws SQLException {
        String stmnt = SELECT_FROM_TABLE_QUERY + tableName;
        try (
                Connection connection = DriverManager.getConnection(url);
                PreparedStatement select = connection.prepareStatement(stmnt);
                ResultSet selectResult = select.executeQuery()
        ) {
            Map<String, String> columns = getColumnsMap(url, tableName);

            Set rowData = new HashSet();
            while (selectResult.next()) {
                Map<String, String> data = new HashMap<String, String>();
                for (Map.Entry<String, String> cols : columns.entrySet()) {
                    data.put(cols.getKey(), getValue(cols.getKey(), cols.getValue(), selectResult));
                }
                rowData.add(data);
            }
            return rowData;
        } catch (SQLException ex) {
            logger.error("Error in connecting to database");
            throw new SQLException();
        }
    }

    /**
     * <p> Gets query for entity type
     * </p>
     *
     * @param entityType type of entity to get query for
     * @return query
     */
    private String getQueryByType(EntityType entityType) {
        switch (entityType) {
            case SCHEMA:
                return SCHEMA_QUERY;
            case TABLE:
                return TABLE_QUERY;
            case COLUMN:
                return COLUMN_QUERY;
            default:
                return "";
        }
    }

    /**
     * <p> Gets value of column
     * </p>
     *
     * @param columnName   name of column to get value from
     * @param value        name of value to get
     * @param selectResult result set to get data from
     * @return column value
     */
    private String getValue(String columnName, String value, ResultSet selectResult) throws SQLException {
        switch (value) {
            case "name":
            case "character varying":
            case "text":
                return selectResult.getString(columnName);
            case "bigint":
            case "smallint":
            case "integer":
                return String.valueOf(selectResult.getLong(columnName));
            case "boolean":
                return String.valueOf(selectResult.getBoolean(columnName));
            default:
                return "";
        }
    }

}
