package cz.pkupka.dbbrowser.repositories;

import cz.pkupka.dbbrowser.models.DBConnection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DBConnectionRepository extends CrudRepository<DBConnection, Long> {

    DBConnection findByName(String databaseName);

}
