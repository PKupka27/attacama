package cz.pkupka.dbbrowser.services;

import cz.pkupka.dbbrowser.helpers.URLBuilder;
import cz.pkupka.dbbrowser.models.DBConnection;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
 * Service class to work with table data
 *
 * @author PKupka27
 *
 */
@Service
public class TableDataService extends AbstractService  {

    /**
     * <p> Finds data for specific table
     * </p>
     * @param tableName name of table to get data from
     * @param connectionName name of connection
     * @return response entity with table data
     */
    public ResponseEntity getTableData(String tableName, String connectionName) {
        DBConnection dbConnection = dbConnectionRepository.findByName(connectionName);
        try {
            return ResponseEntity.ok(customDatabaseDAO.getDataOfTable((URLBuilder.buildURL(dbConnection)), tableName));
        } catch (SQLException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
