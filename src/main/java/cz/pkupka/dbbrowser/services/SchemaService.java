package cz.pkupka.dbbrowser.services;

import cz.pkupka.dbbrowser.helpers.URLBuilder;
import cz.pkupka.dbbrowser.models.DBConnection;
import cz.pkupka.dbbrowser.models.EntityType;
import cz.pkupka.dbbrowser.models.dto.SchemaDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Set;

/**
 * Service class to work with schemas
 *
 * @author PKupka27
 *
 */
@Service
public class SchemaService extends AbstractService {

    /**
     * <p> Finds connection and gets all schemas of the connection
     * </p>
     * @param connectionName name of connection
     * @return response entity with schemas
     */
    public ResponseEntity getSchemasByConnectionName(String connectionName) {
        DBConnection dbConnection = dbConnectionRepository.findByName(connectionName);
        return getSchemas(dbConnection);
    }

    /**
     * <p> Gets schemas from database
     * </p>
     * @param dbConnection object with connetion data
     * @return response entity with schemas
     */
    private ResponseEntity getSchemas(DBConnection dbConnection) {
        try {
            Set<SchemaDTO> schemas = customDatabaseDAO.getAllEntities(URLBuilder.buildURL(dbConnection), EntityType.SCHEMA);
            return ResponseEntity.ok(schemas);
        } catch (SQLException ex) {
            return ResponseEntity.notFound().build();
        }

    }
}
