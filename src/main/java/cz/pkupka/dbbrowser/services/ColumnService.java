package cz.pkupka.dbbrowser.services;

import cz.pkupka.dbbrowser.helpers.URLBuilder;
import cz.pkupka.dbbrowser.models.DBConnection;
import cz.pkupka.dbbrowser.models.EntityType;
import cz.pkupka.dbbrowser.models.dto.ColumnDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Set;

/**
 * Service class to work with columns
 *
 * @author PKupka27
 *
 */
@Service
public class ColumnService extends AbstractService {

    /**
     * <p> Finds connection and gets all columns of the connection
     * </p>
     * @param connectionName name of connection
     * @return response entity with columns
     */
    public ResponseEntity getColumnsByConnectionName(String connectionName) {
        DBConnection dbConnection = dbConnectionRepository.findByName(connectionName);
        return getColumns(dbConnection);
    }

    /**
     * <p> Gets columns from database
     * </p>
     * @param dbConnection object with connetion data
     * @return response entity with columns
     */
    private ResponseEntity getColumns(DBConnection dbConnection) {
        try {
            Set<ColumnDTO> columns = customDatabaseDAO.getAllEntities(URLBuilder.buildURL(dbConnection), EntityType.COLUMN);
            return ResponseEntity.ok(columns);
        } catch (SQLException ex) {
            return ResponseEntity.notFound().build();
        }
    }
}
