package cz.pkupka.dbbrowser.services;

import cz.pkupka.dbbrowser.repositories.CustomDatabaseDAO;
import cz.pkupka.dbbrowser.repositories.DBConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractService {

    protected CustomDatabaseDAO customDatabaseDAO;

    protected DBConnectionRepository dbConnectionRepository;

    @Autowired
    public void setCustomDatabaseDAO(CustomDatabaseDAO customDatabaseDAO) {
        this.customDatabaseDAO = customDatabaseDAO;
    }

    @Autowired
    public void setDbConnectionRepository(DBConnectionRepository dbConnectionRepository) {
        this.dbConnectionRepository = dbConnectionRepository;
    }
}
