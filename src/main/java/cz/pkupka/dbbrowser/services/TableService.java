package cz.pkupka.dbbrowser.services;

import cz.pkupka.dbbrowser.helpers.URLBuilder;
import cz.pkupka.dbbrowser.models.DBConnection;
import cz.pkupka.dbbrowser.models.EntityType;
import cz.pkupka.dbbrowser.models.dto.TableDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Set;

/**
 * Service class to work with tables
 *
 * @author PKupka27
 *
 */
@Service
public class TableService extends AbstractService {

    /**
     * <p> Finds connection and gets all tables of the connection
     * </p>
     * @param connectionName name of connection
     * @return response entity with tables
     */
    public ResponseEntity getTablesByConnectionName(String connectionName) {
        DBConnection dbConnection = dbConnectionRepository.findByName(connectionName);
        return getTables(dbConnection);
    }

    /**
     * <p> Gets tables from database
     * </p>
     * @param dbConnection object with connetion data
     * @return response entity with tables
     */
    private ResponseEntity getTables(DBConnection dbConnection) {
        try {
            Set<TableDTO> tables = customDatabaseDAO.getAllEntities(URLBuilder.buildURL(dbConnection), EntityType.TABLE);
            return ResponseEntity.ok(tables);
        } catch (SQLException ex) {
            return ResponseEntity.notFound().build();
        }
    }
}
